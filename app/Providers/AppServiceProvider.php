<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\TripToCarbon\TripToCarbonInterface;
use App\Repositories\TripToCarbon\EloquentTripToCarbon;
use App\Repositories\ApiCall\ApiCall;
use App\Repositories\ApiCall\ApiCallInterface;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TripToCarbonInterface::class, EloquentTripToCarbon::class);
        $this->app->singleton(ApiCallInterface::class, ApiCall::class);
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
   
}