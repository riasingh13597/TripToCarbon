<?php

namespace App\Http\Controllers;
use Config;
use Illuminate\Http\Request;
use App\Repositories\TripToCarbon\TripToCarbonInterface;
use Illuminate\Support\Facades\Validator;

class TripToCarbonController extends Controller
{
    private $trip_to_carbon;
    public $response;

    public function __construct(TripToCarbonInterface $trip_to_carbon)
    {
        $this->trip_to_carbon = $trip_to_carbon;
    }

    public function create(Request $request)
    {
        
        try{
            /******************************validate request**********************************/
            $validator = $this->validateRequest($request);    
               
            if($validator->fails())
            {
                return response()->json(['data'=>[],'message'=>$validator->errors()->toArray()],422);
            }

            /*****************Based on Activity type call repository***************************/       
            switch ($request->activityType) {
                case Config::get('activityType.activity_type.miles'):
                    $activity    =   $request->activity;
                    $country     =   $request->country;
                    $mode_type   =   $request->mode;
                    $response    =   $this->trip_to_carbon->milesCarbonFootprint($activity,$country,$mode_type);
                    break;
                case Config::get('activityType.activity_type.fuel'):
                    $activity   =    $request->activity;
                    $country    =    $request->country;
                    $fuelType  =     $request->fuelType;
                    $response   =    $this->trip_to_carbon->fuelCarbonFootprint($activity,$country,$fuelType);
                    break;
                default:
                    break;
            }
            
            return response()->json(['data'=>$response,'message'=>"Data Fetch Successfully"],200);
             
        }catch(\Exception $ex)
        {
            return response()->json(['data'=>[], 'message'=> $ex->getMessage()],500);
        }

    }

    private function validateRequest($request)
    {
        $rules = [
            'activity'        =>  'required|numeric|min:1',
            'activityType'   =>  'required|in:fuel,miles',
            'fuelType'       =>  'required_if:activity_type,==,fuel|in:motorGasoline,diesel,aviationGasolin,jetFuel',
            'mode'            =>  'required_if:activity_type,==,miles|in:dieselCar,petrolCar,anyCar,taxi,economyFlight,businessFlight,firstclassFlight,anyFlight,motorbike,bus,transitRail',
            'country'         =>  'required|string|min:3|max:3|regex:/[a-z]/'
        ];

        return Validator::make($request->all(), $rules);
    }
}