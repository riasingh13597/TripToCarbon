<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuelCarbonFootprint extends Model
{
    use HasFactory;
    
    protected $table = "fuel_carbon_footprints";
    protected $fillable = ['activity','fuel_type','country','carbonFootprint'];

    
}