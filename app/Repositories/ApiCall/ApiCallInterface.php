<?php
namespace App\Repositories\ApiCall;

interface ApiCallInterface{
    
    public function FuelParameterApiCall($activity,$country,$fuelType);
    
    public function MilesParameterApiCall($activity,$country,$mode_type);
}