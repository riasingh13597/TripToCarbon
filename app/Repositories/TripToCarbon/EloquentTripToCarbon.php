<?php
namespace App\Repositories\TripToCarbon;
use App\Repositories\TripToCarbon\TripToCarbonInterface;
use App\Models\FuelCarbonFootprint;
use App\Models\MilesCarbonFootprint;
use App\Repositories\ApiCall\ApiCallInterface;
use Cache;
class EloquentTripToCarbon implements TripToCarbonInterface
{
    private $api_call;
    public function __Construct(ApiCallInterface $api_call)
    {
        $this->api_call = $api_call;
    }
    
    
/***********************************Create FuelCarbonFootprint****************************************/
    
    public function fuelCarbonFootprint($activity,$country,$fuelType)
    {
       
        $fuel_cache = Cache::get($country.'_'.$fuelType.'_'.$activity);
       
        if(isset($fuel_cache))
        {
            return $fuel_cache;
        }
        
        /**********************************Calling External API**********************************/
        $result = $this->api_call->FuelParameterApiCall($activity,$country,$fuelType);
        
        $fuel_footprint_created = FuelCarbonFootprint::create([
                                    'activity'        =>   $activity,
                                    'fuel_type'       =>   $fuelType,
                                    'country'         =>   $country,
                                    'carbonFootprint' =>   $result['carbonFootprint']
                                ]);
                
        
        Cache::put($country.'_'.$fuelType.'_'.$activity,$result['carbonFootprint'],24*60*60);
        
        return $result;
    }


/***********************************Create MilesCarbonFootprint****************************************/
    
    public function milesCarbonFootprint($activity,$country,$mode_type)
    {
        $miles_cache = Cache::get($country.'_'.$mode_type.'_'.$activity);
       
        if(isset($miles_cache))
        {
            return $miles_cache;
        }
        
        /**********************************Calling External API**********************************/
        $result = $this->api_call->MilesParameterApiCall($activity,$country,$mode_type);
        
        $Miles_footprint_created = MilesCarbonFootprint::create([
                                    'activity'        =>   $activity,
                                    'mode_type'       =>   $mode_type,
                                    'country'         =>   $country,
                                    'carbonFootprint' =>   $result['carbonFootprint']
                                ]);
        
        Cache::put($country.'_'.$mode_type.'_'.$activity,$result['carbonFootprint'],24*60*60);
        
        return $result;
    }

    
}