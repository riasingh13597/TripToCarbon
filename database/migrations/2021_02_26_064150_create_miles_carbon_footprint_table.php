<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMilesCarbonFootprintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('miles_carbon_footprints', function (Blueprint $table) {
            $table->id();
            $table->integer('activity');
            $table->string('mode_type');
            $table->string('country');
            $table->string('carbonFootprint');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('miles_carbon_footprint');
    }
}