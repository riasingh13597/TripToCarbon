<?php

namespace Tests\Unit;


use App\Models\FuelCarbonFootprint;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TripToCarbonTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    
    public function test_miles_api_call() {

        $data = [
            'activity'        =>   '10',
            'activityType'    =>   'miles',
            'country'         =>   'usa',
            'mode'            =>   'taxi',
        ];

        $this->json('POST', 'api/trip_to_carbon', $data, ['Accept' => 'application/json'])
             ->assertStatus(200);
    }

    public function test_fuel_api_call() {

        $data = [
            'activity'        =>   '10',
            'activityType'    =>   'fuel',
            'country'         =>   'usa',
            'fuelType'        =>   'diesel',
        ];

        $this->json('POST', 'api/trip_to_carbon', $data, ['Accept' => 'application/json'])
             ->assertStatus(200);
    }

    public function test_fuel_validation() {

        $data = [
            'activity'        =>   '10',
            'activityType'    =>   'fuel',
            'country'         =>   'usa',
            'fuelType'        =>    'xyz'
        ];

        $this->json('POST', 'api/trip_to_carbon', $data, ['Accept' => 'application/json'])
             ->assertStatus(422);
    }

    public function test_miles_validation() {

        $data = [
            'activity'        =>   '10',
            'activityType'    =>   'miles',
            'country'         =>   'usa',
            'mode'            =>   'taxis',
        ];

        $this->json('POST', 'api/trip_to_carbon', $data, ['Accept' => 'application/json'])
             ->assertStatus(422);
    }

}